package client

import (
	"todo/config"
	todo_service "todo/genproto/todo_service"
	"todo/genproto/user_service"

	"google.golang.org/grpc"
)

type IServiceManger interface {
	TodoService() todo_service.TodoServiceClient
	UserService() user_service.UserServiceClient
}

type grpcClient struct {
	todo_service todo_service.TodoServiceClient
	user_service user_service.UserServiceClient
}

func NewGrpcClient(cfg config.Config) (IServiceManger, error) {
	connTodoService, err := grpc.Dial(cfg.GRPC_Service_Host+cfg.GRPC_Service_Port, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return &grpcClient{
		todo_service: todo_service.NewTodoServiceClient(connTodoService),
		user_service: user_service.NewUserServiceClient(connTodoService),
	}, nil
}

func (g *grpcClient) TodoService() todo_service.TodoServiceClient{
		return g.todo_service
}

func (g *grpcClient) UserService() user_service.UserServiceClient{
	return g.user_service
}