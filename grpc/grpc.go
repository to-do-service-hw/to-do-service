package grpc

import (
	todo_service "todo/genproto/todo_service"
	"todo/genproto/user_service"
	"todo/grpc/client"
	"todo/service"
	"todo/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(str storage.IStorage, services client.IServiceManger) *grpc.Server {
	grpcServer := grpc.NewServer()

	todo_service.RegisterTodoServiceServer(grpcServer, service.NewTodoService(str, services))
	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(str, services))

	reflection.Register(grpcServer)

	return grpcServer
}
