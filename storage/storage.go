package storage

import (
	"context"
	todo_service "todo/genproto/todo_service"
	"todo/genproto/user_service"
)


type IStorage interface {
	Close()
	Todo() ITodoStorage
	User() IUserStorage
}

type ITodoStorage interface {
	Create(context.Context, *todo_service.CreateTodoRequest) (*todo_service.Todo, error)
	Get(context.Context, *todo_service.PrimaryKey) (*todo_service.Todo, error)
}

type IUserStorage interface{
	Create(context.Context, *user_service.CreateUser) (*user_service.User, error)
}