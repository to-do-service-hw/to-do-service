package postgres

import (
	"context"
	"todo/genproto/user_service"
	"todo/pkg/logger"
	"todo/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type userRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	user_service.UnimplementedUserServiceServer
}

func NewUserRepo(db *pgxpool.Pool, log logger.ILogger) storage.IUserStorage {
	return &userRepo{db: db, log: log}
}

func (u *userRepo) Create(ctx context.Context, request *user_service.CreateUser) (*user_service.User, error) {
	user := user_service.User{}
	query := `insert into users(id, full_name, age)
					values($1, $2, $3)
					returning id, full_name, age`
	if err := u.db.QueryRow(ctx, query, uuid.New(), request.FullName, request.Age).Scan(
		&user.Id,
		&user.FullName,
		&user.Age,
	); err != nil {
		u.log.Error("error is while creating user", logger.Error(err))
		return &user_service.User{}, err
	}

	return &user, nil
}
