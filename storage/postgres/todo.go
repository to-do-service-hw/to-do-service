package postgres

import (
	"context"
	todo_service "todo/genproto/todo_service"
	"todo/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type TodoRepo struct {
	db *pgxpool.Pool
	todo_service.UnimplementedTodoServiceServer
	log logger.ILogger
}

func NewTodoRepo(db *pgxpool.Pool, log logger.ILogger) *TodoRepo {
	return &TodoRepo{db: db, log: log}
}

func (t *TodoRepo) Create(ctx context.Context, request *todo_service.CreateTodoRequest) (*todo_service.Todo, error) {
	id := uuid.New()
	todo := todo_service.Todo{}
	query := `insert into todos(id, name, deadline, status)
					values($1, $2, $3, $4) 
					returning id, name, deadline, status, updated_at::text, created_at::text`
	if err := t.db.QueryRow(ctx, query, id, request.Name, request.Deadline, request.Status).Scan(
		&todo.Id,
		&todo.Name,
		&todo.Deadline,
		&todo.Status,
		&todo.UpdatedAt,
		&todo.CreatedAt,
	); err != nil {
		t.log.Error("error is while creating todo", logger.Error(err))
		return &todo_service.Todo{}, err
	}
	return nil, nil
}

func (t *TodoRepo) Get(ctx context.Context, request *todo_service.PrimaryKey) (*todo_service.Todo, error) {
	return nil, nil
}
