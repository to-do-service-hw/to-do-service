package postgres

import (
	"context"
	"fmt"
	"todo/config"
	"todo/pkg/logger"
	"todo/storage"

	"github.com/jackc/pgx/v5/pgxpool"

	pb "todo/genproto/todo_service"

	_"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database"          //database is needed for migration
	_ "github.com/golang-migrate/migrate/v4/database/postgres" //postgres is used for database
	_ "github.com/golang-migrate/migrate/v4/source/file"       //file is needed for migration url
	_ "github.com/lib/pq"
)

type Store struct {
	db  *pgxpool.Pool
	log logger.ILogger
	cfg config.Config
	pb.UnimplementedTodoServiceServer
}

func New(ctx context.Context, log logger.ILogger, cfg config.Config) (Store, error) {
	url := fmt.Sprintf(`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)
	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		log.Error("error is while parse config", logger.Error(err))
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		log.Error("error is while new with config", logger.Error(err))
		return Store{}, err
	}

	// m, err := migrate.New("file:/migrations", url)
	// if err != nil {
	// 	log.Error("error is while migrate", logger.Error(err))
	// 	return Store{}, err
	// }

	// if err := m.Up(); err != nil {
	// 	log.Error("error is while migrating up", logger.Error(err))
	// 	return Store{}, err
	// }

	return Store{
		db:  pool,
		cfg: cfg,
		log: log,
	}, nil
}

func (s *Store) Close() {
	s.db.Close()
}

func (s *Store) Todo() storage.ITodoStorage {
return NewTodoRepo(s.db, s.log)
}

func (s *Store) User() storage.IUserStorage {
	return NewUserRepo(s.db, s.log)
}