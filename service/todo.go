package service

import (
	"context"
	todo_service "todo/genproto/todo_service"
	"todo/grpc/client"
	"todo/storage"
)

type todoService struct {
	storage     storage.IStorage
	services client.IServiceManger
	todo_service.UnimplementedTodoServiceServer
}

func NewTodoService(strg storage.IStorage, services client.IServiceManger) *todoService {
	return &todoService{storage: strg, services: services}
}

func (t *todoService) Create(ctx context.Context, request *todo_service.CreateTodoRequest) (*todo_service.Todo, error) {
	return t.storage.Todo().Create(ctx, request)
}
