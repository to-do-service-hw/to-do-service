package service

import (
	"context"
	"todo/genproto/user_service"
	"todo/grpc/client"
	"todo/storage"
)

type userService struct {
	strg     storage.IStorage
	services client.IServiceManger
	user_service.UnimplementedUserServiceServer
}

func NewUserService(strg storage.IStorage, services client.IServiceManger) *userService {
	return &userService{
		strg:     strg,
		services: services,
	}
}

func (u *userService) Create(ctx context.Context, request *user_service.CreateUser) (*user_service.User, error) {
	return u.strg.User().Create(ctx, request)
}
