create if not exist table  todos(
    id uuid primary key,
    name varchar(30),
    status bool default false,
    deadline varchar(30),
    created_at timestamp default now(),
    updated_at timestamp default now()
);


create table users(
    id uuid primary key,
    full_name varchar(30),
    age int
);