package config

import (
	"log"
	"os"
	_ "os"

	"github.com/spf13/cast"
	_ "github.com/spf13/cast"

	"github.com/joho/godotenv"
)
type Config struct {
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string
	
	ServiceName string
	Environment string
	LoggerLevel string

	GRPC_Service_Host string
	GRPC_Service_Port string
}

func Load() Config{
	if err := godotenv.Load(); err != nil {
		log.Println("error is while loading env", err.Error())
	}

	cfg := Config{}

	cfg.PostgresHost = cast.ToString(getOrReturnDefaultValue("POSTGRES_HOST", "localhost"))
	cfg.PostgresPort = cast.ToString(getOrReturnDefaultValue("POSTGRES_PORT", "5432"))
	cfg.PostgresUser = cast.ToString(getOrReturnDefaultValue("POSTGRES_USER", "user"))
	cfg.PostgresPassword = cast.ToString(getOrReturnDefaultValue("POSTGRES_PASSWORD", "password"))
	cfg.PostgresDB = cast.ToString(getOrReturnDefaultValue("POSTGRES_DB", "database"))

	cfg.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "todo-service-hw"))
	cfg.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", "dev"))
	cfg.LoggerLevel = cast.ToString(getOrReturnDefaultValue("LOGGER_LEVEL", "debug"))
	
	cfg.GRPC_Service_Host = cast.ToString(getOrReturnDefaultValue("GRPC_SERVICE_HOST", "localhost"))
	cfg.GRPC_Service_Port = cast.ToString(getOrReturnDefaultValue("GRPC_SERVICE_PORT", ":8001"))

	return cfg
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{}{
	value := os.Getenv(key)

	if value != "" {
		return value
	}

	return defaultValue
}
