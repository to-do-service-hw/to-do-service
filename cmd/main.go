package main

import (
	"context"
	"net"
	"todo/config"
	"todo/grpc"
	"todo/grpc/client"
	"todo/pkg/logger"
	"todo/storage/postgres"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postgres.New(context.Background(), log, cfg)
	if err != nil {
		log.Error("error is while connecting db", logger.Error(err))
		return
	}
	defer pgStore.Close()

	services, err := client.NewGrpcClient(cfg)
	if err != nil {
		log.Error("error is while new grpc client", logger.Error(err))
		return
	}

	grpcServer := grpc.SetUpServer(&pgStore, services)

	lis, err := net.Listen("tcp", cfg.GRPC_Service_Host+cfg.GRPC_Service_Port)
	if err != nil {
		log.Error("error is while listening", logger.Error(err))
		return
	}


	log.Info("Server running...... ", logger.Any("port: ", cfg.GRPC_Service_Port))
	if err = grpcServer.Serve(lis); err != nil {
		log.Error("error is while server list", logger.Error(err))
		return
	}
}
